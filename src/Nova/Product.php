<?php

namespace Nitm\Transactions\Nova;

use Laravel\Nova\Panel;
use Spatie\TagsField\Tags;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Http\Requests\NovaRequest;

class Product extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \Nitm\Transactions\Models\Product::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The relationships that should be eager loaded when performing an index query.
     *
     * @var array
     */
    public static $with = [];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Products';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'title',  'description'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(),
            Text::make(__('Title'), 'title')->sortable(),
            Text::make(__('Stripe Product Id'), 'stripe_product_id'),
            Number::make(__('Price'), function () {
                return money($this->price ?: 0, $this->currency)->render();
            }),
            Number::make(__('Price'), 'price'),
            Number::make(__('Points'), 'points')->sortable(),
            Number::make('# Transactions', 'transactions_count')
                ->onlyOnIndex()
                ->sortable(),
            new Panel("Transactions", $this->relatedFields()),
        ];
    }

    public function relatedFields()
    {
        return [
            HasMany::make(__('Transactions'), 'transactions', \Nitm\Transactions\Nova\Reputation::class)->readOnly(),
        ];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        // adds a `tags_count` column to the query result based on
        // number of tags associated with this product
        return $query->withCount('transactions');
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
