<?php

namespace Nitm\Transactions\Notifications;

use Nitm\Transactions\Notifications\BaseNotification;

class AdminTransactionWasSuccessful extends BaseNotification
{
}
