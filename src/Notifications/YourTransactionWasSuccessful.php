<?php

namespace Nitm\Transactions\Notifications;

use Illuminate\Bus\Queueable;
use Nitm\Transactions\Notifications\BaseNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class YourTransactionWasSuccessful extends BaseNotification
{
}
