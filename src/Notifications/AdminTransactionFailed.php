<?php

namespace Nitm\Transactions\Notifications;

use Nitm\Transactions\Notifications\BaseNotification;

class AdminTransactionFailed extends BaseNotification
{
}
