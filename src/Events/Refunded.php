<?php

namespace Nitm\Transactions\Events;

use Illuminate\Broadcasting\PrivateChannel;
use Nitm\Content\Events\BaseAutomationEvent;
use Nitm\Transactions\Contracts\Models\Transaction;

class Refunded extends BaseAutomationEvent
{
    public $points;

    /**
     * @var Transaction
     *
     * @var [type]
     */
    public $transaction;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Transaction $transaction, $points = null)
    {
        $transaction->payee_id = $transaction->payee_id ?: optional(auth()->user())->id;
        $this->transaction = $transaction;
        $this->points = $points ?: $transaction->point;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('users.' . $this->transaction->getUserId());
    }
}
