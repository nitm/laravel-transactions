<?php

namespace Nitm\Transactions\Events;

use Illuminate\Broadcasting\PrivateChannel;
use Nitm\Content\Events\BaseAutomationEvent;
use Nitm\Transactions\Contracts\Models\Transaction;

class Succeeded extends BaseAutomationEvent
{
    public $points;

    /**
     * @var Transaction
     *
     * @var [type]
     */
    public $transaction;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Transaction $Transaction, $points = null)
    {
        $Transaction->payee_id = $Transaction->payee_id ?: optional(auth()->user())->id;
        $this->transaction = $Transaction;
        $this->points = $points ?: $Transaction->point;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('users.' . $this->transaction->payee_id);
    }
}