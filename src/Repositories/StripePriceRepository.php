<?php

/**
 * Copyright 2020 Cloud Creativity Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Nitm\Transactions\Repositories;

use Stripe\Price;
use Nitm\Transactions\Facades\Stripe;
use CloudCreativity\LaravelStripe\Assert;
use CloudCreativity\LaravelStripe\Repositories\Concerns\All;
use CloudCreativity\LaravelStripe\Repositories\Concerns\Update;
use CloudCreativity\LaravelStripe\Repositories\Concerns\Retrieve;
use CloudCreativity\LaravelStripe\Repositories\AbstractRepository;

class StripePriceRepository extends AbstractRepository
{

    use All;
    use Retrieve;
    use Update;

    /**
     * Create a charge.
     *
     * Both currency and amount are required parameters.
     *
     * @param string $currency
     * @param int $amount
     * @param iterable|array $params
     *      additional optional parameters.
     * @return Charge
     */
    public function create(string $product, $unitAmount = 1, $currency = 'usd', iterable $params = []): Price
    {
        if (app()->environment('testing')) {
            Stripe::fake($model = new Price('price_00000000000000', [
                'product' => $product,
                'unit_amount' => $unitAmount,
                'currency' => $currency,
            ]));
            return $model;
        }
        $currency = strtolower($currency);
        $unitAmount = intval($unitAmount);
        Assert::supportedCurrency($currency);
        Assert::zeroDecimal($unitAmount);
        $this->params($params)->params([
            'product' => $product,
            'unit_amount' => $unitAmount,
            'currency' => $currency,
        ]);

        return $this->send(
            'create',
            $this->params ?: null,
            $this->options ?: null
        );
    }

    /**
     * @inheritDoc
     */
    protected function fqn(): string
    {
        return Price::class;
    }
}
