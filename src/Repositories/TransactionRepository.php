<?php

/**
 * Transaction Repository
 */

namespace Nitm\Transactions\Repositories;

use Illuminate\Support\Arr;
use Nitm\Transactions\Models\User;
use Illuminate\Support\Facades\Log;
use Nitm\Transactions\Models\Product;
use Nitm\Transactions\Events\Refunded;
use Nitm\Transactions\Events\Succeeded;
use Nitm\Transactions\Models\Transaction;
use Nitm\Content\Repositories\BaseRepository;
use Nitm\Transactions\Gamify\Points\Purchase;
use CloudCreativity\LaravelStripe\Facades\Stripe;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class TransactionRepository
 *
 * @package App\Repositories
 * @version January 16, 2021, 8:42 pm UTC
 */

class TransactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model(): string
    {
        return Transaction::class;
    }

    /**
     * Gift Points to a user
     *
     * @param array $input
     *
     * @return Model Return an up to date fresh model
     */
    public function refund($input)
    {
        $transaction = Transaction::findOrFail(Arr::get($input, 'transaction_id'));
        $product     = Product::whereStripeProductId($transaction->stripe_product_id)->first();
        $user        = auth()->user();

        try {
            if (app()->environment('testing')) {
                $refund = optional(['id' => 'test_refund']);
            } else {
                $refund = Stripe::account()->refunds()->create(Stripe::account()->charges()->retrieve($transaction->stripe_charge_id)->id);
            }
            $transaction->point -= $transaction->point;
            $transaction->stripe_refund_id = $refund->id;
            $transaction->save();

            event(new Refunded($transaction, $product->points));
        } catch (\Exception $e) {
            \Log::error($e);
            if (app()->environment('local', 'dev', 'testing')) {
                throw $e;
            }
        }

        return $transaction;
    }

    /**
     * Create points for a user
     *
     * @param array $input
     *
     * @return Model Return an up to date fresh model
     */
    public function create($input)
    {
        $user    = auth()->user();
        $points  = Arr::get($input, 'points', Arr::get($input, 'point'));
        $product = Product::find(Arr::get($input, 'product_id'));
        if (!$product && !$points) {
            throw new ModelNotFoundException('Product not found for id: ' . Arr::get($input, 'product_id'));
        }
        $input['payee_id'] = Arr::get($input, 'payee_id') ?: $user->id;
        $input['point']    = $points ?: $product->point;
        $input['amount']   = Arr::get($input, 'amount', $product ? $product->price : 0);
        $input['currency'] = Arr::get($input, 'currency', config('stripe.currencies', ['USD'])[0]);
        $result = parent::create($input);

        event(new Succeeded($result, $input['point']));

        return $result;
    }
}