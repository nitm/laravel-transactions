<?php
/**
 * Product Repository
 */
namespace Nitm\Transactions\Repositories;

use Nitm\Transactions\Models\Product;
use Nitm\Content\Repositories\BaseRepository;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @version January 16, 2022, 3:48 am UTC
*/

class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model(): string
    {
        return Product::class;
    }
}
