<?php

namespace Nitm\Transactions\Jobs;

use Nitm\Transactions\Facades\Stripe;


class UpdateStripeProduct extends CreateStripeProduct
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (app()->environment('testing')) {
            return;
        }
        if ($this->product->stripe_product_id) {
            Stripe::account()->products()->update($this->product->stripe_product_id, [
                'active' => !$this->deleted_at,
                'name' => $this->product->title,
                'description' => $this->product->description
            ]);

            $prices = Stripe::account()->prices()->all(['product' => $this->product->stripe_product_id]);
            if (count($prices->data)) {
                Stripe::account()->prices()->update($prices->data[0]->id, [
                    'price' => $this->product->price,
                    'currency' => $this->product->currency ?: config('stripe.currencies')[0]
                ]);
            }
        }
    }
}
