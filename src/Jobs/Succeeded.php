<?php

namespace Nitm\Transactions\Jobs;

use CloudCreativity\LaravelStripe\Models\StripeAccount;
use CloudCreativity\LaravelStripe\Webhooks\Webhook;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Nitm\Transactions\Models\Product;
use Nitm\Transactions\Models\Transaction;
use Nitm\Transactions\Models\User;

class Succeeded implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Webhook
     */
    public $webhook;

    /**
     * @var Transaction
     */
    protected $transaction;

    public function __construct(Webhook $webhook)
    {
        $this->webhook = $webhook;
    }

    public function handle()
    {
        $amount = $this->webhook->webhook->data->object->amount;
        if (!($transaction = $this->getTransaction())) {
            \Log::error('Transaction not found for charge id: ' . $this->webhook->webhook->data->object->id);
            return;
        }
        $owner = $this->findUser($this->webhook->webhook->data->object->billing_details->email);
        if (!$owner) {
            throw new ModelNotFoundException("Unable to find the user for this charge");
        }
        auth()->login($owner);
        $product = Product::wherePrice($amount)->first();

        if ($product) {
            $transaction->fill([
                'point'             => $product->points,
                'product_id'        => $product->id,
                'stripe_product_id' => $product->stripe_product_id,
                'stripe_charge_id'  => $this->webhook->webhook->data->object->id,
            ]);
            $this->transaction->save();
        } else {
            \Log::error("Unable to find product with price: " . $amount);
            if (app()->environment('local', 'dev')) {
                throw new ModelNotFoundException("Unable to find product with price: " . $amount);
            }
        }

        return $result;
    }

    /**
     * Get the transaction for this request
     *
     * @return Transaction
     */
    protected function getTransaction(): Transaction
    {
        if (!$this->transation && $pi = $this->webhook->object->payment_intent) {
            $this->transaction = Transaction::whereStripePaymentIntentId($pi)->first();
        }
        return $this->transaction;
    }

    /**
     * Find the user associated witht he transaaction
     *
     * @param  mixed $email
     * @return User
     */
    protected function findUser($email = null): User
    {
        $user = null;
        if ($this->transaction) {
            $user = $this->transaction->payee;
        } else {
            if ($this->webhook->model->account_id) {
                $user = $this->webhook->model->account->owner;
            } else if (!empty($email)) {
                $account = StripeAccount::whereEmail($email)->first();
                if ($account) {
                    $user = $account->owner;
                } else {
                    $user = User::whereEmail($email)->first();
                }
            }
            if ($user) {
                $this->transaction->payee_id = $user->id;
            }
        }
        return $user;
    }
}
