<?php

namespace Nitm\Transactions\Jobs;

use Nitm\Transactions\Facades\Stripe;

class ArchiveStripeProduct extends CreateStripeProduct
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!(app()->environment('testing'))) {
            Stripe::account()->products()->update($this->product->stripe_product_id, [
                'active' => false
            ]);
        }
    }
}
