<?php

namespace Nitm\Transactions\Jobs;

use Nitm\Transactions\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Nitm\Transactions\Facades\Stripe;

class CreateStripeProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $product;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (app()->environment('testing')) {
            Stripe::fake(
                $product = new \Stripe\Product(),
                $price = new \Stripe\Price()
            );
        } else {
            $product = Stripe::account()->products()->create($this->product->title, [
                'description' => $this->product->description
            ]);
        }

        $this->product->stripe_product_id = $product->id;
        $this->product->save();

        if (!app()->environment('testing')) {
            Stripe::account()->prices()->create($product->id, $this->product->price, $this->product->currency ?: config('stripe.currencies')[0], [
                'tax_behavior' => 'inclusive'
            ]);
        }
    }
}
