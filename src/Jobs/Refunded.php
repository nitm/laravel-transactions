<?php

namespace Nitm\Transactions\Jobs;

use CloudCreativity\LaravelStripe\Webhooks\Webhook;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Nitm\Transactions\Models\Transaction;
use Nitm\Transactions\Models\User;
use Nitm\Transactions\Repositories\TransactionRepository;

class Refunded implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Webhook
     */
    public $webhook;

    public function __construct(Webhook $webhook)
    {
        $this->webhook = $webhook;
    }

    public function handle()
    {
        $id    = $this->webhook->webhook->data->object->id;
        $owner = $this->webhook->model->owner ?: User::first();
        auth()->login($owner);
        $transaction = Transaction::whereStripeChargeId($id)->first();
        if ($transaction) {
            app(TransactionRepository::class)->refund([
                'transaction_id' => $transaction->id,
            ]);
        } else {
            \Log::error("Unable to find transaction with stripe_charge_id: " . $id);
            if (app()->environment('local', 'dev')) {
                throw new ModelNotFoundException("Unable to find transaction with id: " . $id);
            }
        }
    }
}
