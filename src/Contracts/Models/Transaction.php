<?php

namespace Nitm\Transactions\Contracts\Models;

interface Transaction
{
    /**
     * Get the userid of the user who owns the transaction
     *
     * @return void
     */
    public function getUserId(): int|string;

    /**
     * Supports a converstion between trasnsaction value and application points
     *
     * @return void
     */
    public function getPoints(): int|string;
}