<?php

namespace Nitm\Transactions\Http\Controllers;

use Exception;
use Stripe\Event;
use Illuminate\Http\Request;
use Nitm\Content\Models\User;
use Illuminate\Database\Eloquent\Model;
use Nitm\Transactions\Models\Transaction;
use Nitm\Transactions\Jobs\Transactions\Refunded;
use Nitm\Transactions\Jobs\Transactions\Succeeded;
use CloudCreativity\LaravelStripe\Webhooks\Webhook;
use CloudCreativity\LaravelStripe\Models\StripeAccount;

/**
 * Controller
 * Extend with extra functionality as needed
 * Used for simulating scenarios through API calls. Should not be acccessible to the public or in production.
 *
 * @author Malcolm Paul <malcolm@ninjasitm.com>
 */
class DevController extends Controller
{
    /**
     * ComingSoon
     *
     * @return void
     */
    public function index(Request $request)
    {
        $result = [
            'status' => 'error',
            'data'   => [],
        ];

        switch ($request->type) {
            case 'transactions':
                $result['data'] = $this->transactions($request);
                break;
            case 'charge.succeeded':
                $result['data'] = $this->stripeSucceeded($request);
                break;
            case 'charge.refunded':
                $result['data'] = $this->stripeRefunded($request);
                break;
        }

        $result['status'] = !empty($result['data']) ? 'success' : 'error';
        return response()->json($result);
    }

    /**
     * Test Stripe Succeeded webhook
     *
     * @param  mixed $request
     * @return void
     */
    protected function stripeSucceeded(Request $request)
    {
        $result = Succeeded::dispatchNow(new Webhook(Event::constructFrom($request->all()), optional()));
        return $result;
    }

    /**
     * Test Stripe Succeeded webhook
     *
     * @param  mixed $request
     * @return void
     */
    protected function stripeRefunded(Request $request)
    {
        $account           = new StripeAccount;
        $account->owner_id = transactions::whereStripeChargeId($request->data['object']['id'])->first()->payee_id;
        $result            = Refunded::dispatchNow(new Webhook(Event::constructFrom($request->all()), $account));
        return $result;
    }

    /**
     * transactions
     *
     * @param  mixed $request
     * @return array
     */
    protected function transactions(Request $request): array
    {
        $user   = $this->getUser($request);
        $result = [
            'target' => $this->getTarget($request, $user),
        ];

        if (!$result['target']) {
            throw new Exception('No target for transactions for: ' . $user->name);
        }
        $result['result'] = $user->transactionss()->first();
        return $result;
    }

    /**
     * Get Target
     *
     * @param  mixed $request
     * @param  mixed $user
     * @return Model
     */
    protected function getTarget(Request $request, User $user): Model
    {
        $query = null;
        switch ($request->target_type) {
            default:
                break;
        }
        if ($request->target_id) {
            $query->find($request->target_id);
        }
        return $query->first();
    }

    /**
     * Uet User
     *
     * @param  mixed $request
     * @return User
     */
    protected function getUser(Request $request): User
    {
        return $request->user_id ? User::findOrFail($request->user_id) : User::first();
    }
}
