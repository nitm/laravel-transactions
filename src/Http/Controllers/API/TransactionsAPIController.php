<?php

namespace Nitm\Transactions\Http\Controllers\API;

use Stripe\EphemeralKey;
use Illuminate\Http\Request;
use Nitm\Transactions\Facades\Stripe;
use Nitm\Transactions\Models\Product;
use Illuminate\Support\Facades\Response;
use Nitm\Transactions\Models\Transaction;
use Nitm\Transactions\Gamify\Points\Purchase;
use Nitm\Transactions\Http\Controllers\API\ApiController;
use Nitm\Transactions\Http\Requests\PaymentIntentRequest;
use Nitm\Transactions\Repositories\TransactionRepository;
use Nitm\Transactions\Http\Requests\TransactionGiftRequest;
use Nitm\Transactions\Http\Requests\TransactionRefundRequest;
use CloudCreativity\LaravelStripe\Repositories\AccountRepository;
use Symfony\Component\HttpClient\Exception\InvalidArgumentException;

/**
 * Class TransactionsAPIController
 *
 * Used for documentation only!
 *
 * @package Nitm\Transactions\Http\Controllers\API
 * @author Malcolm Paul <malcolm@ninjasitm.com>
 */

class TransactionsAPIController extends ApiController
{
    /**
     * Get the repository class
     *
     * @return string
     */
    public function repository()
    {
        return AccountRepository::class;
    }

    /**
     * Redirect the user to the Google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $type = 'mobile')
    {
        return Stripe::authorizeUrl()->readWrite();
    }


    /**
     * @param  TransactionGiftRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/api/payments/transactions/refund/{transactionId}",
     *      summary="Refund a transaction",
     *      tags={"Payments"},
     *      description="Refund a transaction",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     * @SWG\Parameter(
     *          name="transactionId",
     *          description="ID of the transaction",
     *          type="number",
     *          required=true,
     *          in="path"
     *      ),
     * @SWG\Response(
     *          response=200,
     *          description="successful operation",
     * @SWG\Schema(
     *              type="object",
     * @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     * @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Transaction"
     *              ),
     * @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function refund(TransactionRefundRequest $request, Transaction $transaction)
    {
        $model = app(TransactionRepoisitory::class)->refund(['transaction_id' => $transaction->id]);

        return $this->printModelSuccess($model, 'Transaction refunded successfully');
    }


    /**
     * @param  TransactionGiftRequest $request
     * @website https://stripe.com/docs/payments/accept-a-payment?platform=react-native#react-native-setup
     * @return Response
     *
     * @SWG\Post(
     *      path="/api/payments/transactions",
     *      summary="Get a payment intent for stripe",
     *      tags={"Payments"},
     *      description="Gift Points",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     * @SWG\Parameter(
     *          name="product_id",
     *          description="The ID of the product",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     * @SWG\Response(
     *          response=200,
     *          description="successful operation",
     * @SWG\Schema(
     *              type="object",
     * @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     * @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PaymentIntent"
     *              ),
     * @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function paymentIntent(PaymentIntentRequest $request)
    {
        $product = Product::findOrFail($request->product_id);
        $stripeProduct = $product->getProduct();
        if (!$stripeProduct) {
            throw new InvalidArgumentException('Stripe product not found!');
        }

        $stripeId = auth()->user()->stripe_id;
        $stripe = app('nitm.stripe')->account()->customers();
        $customer = $stripeId ? $stripe->retrieve($stripeId) : $stripe->create($request->user()->name, $request->user()->email);

        $amount = intval($product->price);

        $paymentIntent = app('nitm.stripe')
            ->account()
            ->paymentIntents()
            ->create(
                strtolower($product->currency ?? config('stripe.currencies')[0]),
                $amount,
                [
                    'application_fee' => $amount * 0.01
                ]
            );

        if (app()->environment('testing')) {
            Stripe::fake($ephemeralKey = new EphemeralKey);
        } else {
            $ephemeralKey = EphemeralKey::create([
                'customer' => $customer->id
            ], [
                'stripe_version' => config('stripe.api_version'),
            ]);
        }

        $model = app(TransactionRepository::class)->create([
            'product_id' => $product->id,
            'point' => $product->points ?: 0,
            'stripe_product_id' => $request->product_id,
            'stripe_payment_intent_id' => $paymentIntent->id,
            'currency' => $product->currency,
            'payee_id' => $request->user()->id,
        ]);

        return $this->printSuccess([
            'payment_intent' => $paymentIntent->client_secret,
            'ephemeral_key' => $ephemeralKey->secret,
            'customer' => $customer->id,
            'publishable_key' => config('stripe.publishable_key'),
        ]);
    }

    /**
     * @param  Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/api/payments/transactions",
     *      summary="Get a listing of the transactions for the current user.",
     *      tags={"Payments"},
     *      description="Get all transaction for the current user",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     * @SWG\Response(
     *          response=200,
     *          description="successful operation",
     * @SWG\Schema(
     *              type="object",
     * @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     * @SWG\Property(
     *                  property="data",
     *                  type="array",
     * @SWG\Items(ref="#/definitions/Transaction")
     *              ),
     * @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function transactions(Request $request)
    {
        return $this->paginate($request, $request->user()->transactions()->search());
    }

    /**
     * Get the user transaction history directly from Stripe
     *
     * @param  mixed $request
     * @return void
     */
    public function stripeTransactions(Request $request)
    {
        return $request->user()->stripe()->charges()->collect();
    }


    /**
     * @param  Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/api/payments/form-config",
     *      summary="Get the form configuration for initiating payments",
     *      tags={"Payments"},
     *      description="Get the form configuration for initiating payments",
     *      produces={"application/json"},
     *      security={{"Bearer":{}}},
     * @SWG\Response(
     *          response=200,
     *          description="successful operation",
     * @SWG\Schema(
     *              type="object",
     * @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     * @SWG\Property(
     *                  property="data",
     *                  type="array",
     * @SWG\Items(type="object")
     *              ),
     * @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function formConfig(Request $request)
    {
        return $this->printSuccess([
            'products' => Product::get()
        ]);
    }
}