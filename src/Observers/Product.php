<?php

namespace Nitm\Transactions\Observers;

use Nitm\Transactions\Jobs\CreateStripeProduct;
use Nitm\Transactions\Jobs\UpdateStripeProduct;
use Nitm\Transactions\Jobs\ArchiveStripeProduct;
use Nitm\Content\Observers\BaseObserver;

class Product extends BaseObserver
{
    public function created($model)
    {
        CreateStripeProduct::dispatch($model);
    }

    public function updated($model)
    {
        UpdateStripeProduct::dispatch($model);
    }

    public function deleting($model)
    {
        ArchiveStripeProduct::dispatch($model);
    }
}