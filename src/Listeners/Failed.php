<?php

namespace Nitm\Transactions\Listeners;

use Nitm\Transactions\Gamify\Points\Purchase;
use Illuminate\Support\Collection;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Nitm\Content\Listeners\BaseAutomationEventListener;

class Failed extends BaseAutomationEventListener
{
    /**
     * Undocumented variable
     *
     * @var string
     */
    protected $notificationClass = 'Nitm\Transactions\Notifications\Failed';

    /**
     * Undocumented variable
     *
     * @var string
     */
    protected $ownerNotificationClass = 'Nitm\Transactions\Notifications\YourTransactionFailed';

    /**
     * Undocumented variable
     *
     * @var string
     */
    protected $adminNotificationClass = 'Nitm\Transactions\Notifications\AdminTransactionFailed';

    /**
     * Get the messge for the user
     *
     * @var string
     */
    protected $message = 'transactions.notify_failed';

    /**
     * Get the messge for the owner
     *
     * @var string
     */
    protected $ownerMessage = 'transactions.notify_owner_failed';

    /**
     * Get the message for the admin
     *
     * @var string
     */
    protected $adminMessage = 'transactions.notify_admin_failed';

    /**
     * Get the messge for the user
     *
     * @var string
     */
    protected $actionText = 'My Transactions';

    /**
     * Get The Type for the notification
     *
     * @return void
     */
    public function getType()
    {
        return 'Transaction';
    }

    /**
     * Get the user from the event
     *
     * @param  mixed $event
     * @return Collection
     */
    protected function getOwner($event): Collection
    {
        return collect([$event->transaction->owner]);
    }

    /**
     * Get the user from the event
     *
     * @param  mixed $event
     * @return Collection
     */
    protected function getUsers($event): Collection
    {
        return $this->getOwner($event);
    }

    /**
     * Get Data
     *
     * @param  mixed $team
     * @param  mixed $event
     * @return array
     */
    public function getData($event): array
    {
        return array_merge($this->getCoreData($event), [
            'action' => 'transaction_failed',
            'transaction' => $event->transaction->toArray(),
            'action_text' => $this->getActionText($event),
            'action_url' => $this->getActionUrl($event)
        ]);
    }

    /**
     * Get the event Params
     *
     * @param  mixed $team
     * @param  mixed $event
     * @return array
     */
    public function getMessageParams($event): array
    {
        return [
            'amount' => money($event->transaction->amount, $event->transaction->currency),
            'points' => $event->transaction->point,
            'userName' => $event->transaction->owner->name,
            'userId' => $event->transaction->owner->id,
        ];
    }

    /**
     * Get the Action Url
     *
     * @param  mixed $team
     * @param  mixed $event
     * @return string
     */
    public function getActionUrl($event): string
    {
        return '/' . implode('/', ['app', 'transactions', $event->transaction->id]);
    }

    /**
     * Get the Action Url For Admin
     *
     * @param  mixed $team
     * @param  mixed $event
     * @return string
     */
    public function getActionUrlForAdmin($event): string
    {
        return '/' . implode('/', ['app', 'transactions', $event->transaction->id]);
    }
}
