<?php

namespace Nitm\Transactions;

use Illuminate\Support\ServiceProvider;
use Nitm\Transactions\Models\Product;
use Nitm\Transactions\Observers\Product as ProductObserver;
use Nitm\Transactions\Stripe\StripeService;

class TransactionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/routes.php');

        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
            $this->publishes([
                __DIR__ . '/../database/migrations' => database_path('migrations'),
            ], 'nitm-transactions-migrations');
            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('nitm-transactions.php'),
            ], 'nitm-transactions-config');
        }

        $this->setupObservers();
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'nitm-transactions');
        $this->app->singleton(StripeService::class);
        $this->app->alias(StripeService::class, 'nitm.stripe');
        // $this->app->alias(CloudCreativityStripeService::class, 'nitm.stripe');
    }

    /**
     * Set the nova user model
     *
     * @param  mixed $class
     * @return void
     */
    public static function useNovaUser($class)
    {
        config(['nitm-transactions.nova.user' => $class]);
    }

    protected function setupObservers()
    {
        Product::observe(ProductObserver::class);
    }
}