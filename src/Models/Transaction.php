<?php

namespace Nitm\Transactions\Models;

use Illuminate\Support\Str;
use Nitm\Content\Models\Model as ModelsModel;
use CloudCreativity\LaravelStripe\Models\StripeEvent;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Database\Factories\Nitm\Transactions\Models\TransactionFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nitm\Transactions\Contracts\Models\Transaction as ModelsTransaction;

/**
 * @SWG\Definition(
 *      definition="PaymentIntent",
 *      @SWG\Property(
 *          property="payment_intent",
 *          description="payment_intent",
 *          type="string",
 *          format="string"
 *      ),
 *      @SWG\Property(
 *          property="ephemeral_key",
 *          description="ephemeral_key",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="customer",
 *          description="customer",
 *          type="string",
 *          format="string"
 *      ),
 *      @SWG\Property(
 *          property="publishable_key",
 *          description="publishable_key",
 *          type="string",
 *          format="string"
 *      ),
 * )
 */
/**
 * @SWG\Definition(
 *      definition="Transaction",
 *      required={"name", "product_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="point",
 *          description="point",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="currency",
 *          description="currency",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="number"
 *      ),
 *      @SWG\Property(
 *          property="stripe_product_id",
 *          description="stripe_product_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *       @SWG\Property(
 *          property="payee",
 *          ref="#/definitions/User"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Transaction extends ModelsModel implements ModelsTransaction
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'points',
        'amount',
        'currency',
        'product_id',
        'stripe_event_id',
        'stripe_invoice_id',
        'stripe_product_id',
        'stripe_charge_id',
        'stripe_refund_id',
        'meta',
        'payee_id',
        'subject_id',
        'subject_type',
    ];

    protected $createdByAuthFields = ['payee_id'];

    protected $appends = ['is_refunded'];

    protected $hidden = ['stripe_event_id', 'stripe_invoice_id', 'stripe_product_id', 'stripe_charge_id', 'stripe_refund_id'];

    protected $casts = [
        'meta' => 'array',
    ];

    /**
     * payee
     *
     * @return BelongsTo
     */
    public function payee(): BelongsTo
    {
        return $this->belongsTo(config('nitm-transactions.user_model'), 'payee_id');
    }

    /**
     * user
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->payee();
    }

    /**
     * transaction
     *
     * @return void
     */
    public function transaction()
    {
        return $this->belongsTo(StripeEvent::class);
    }

    /**
     * Is the transsaction refunded?
     *
     * @return bool
     */
    public function getIsRefundedAttribute(): bool
    {
        return $this->stripe_refund_id !== null;
    }

    /**
     * Get the price
     *
     * @return void
     */
    public function getPriceAttribute()
    {
        return $this->amount;
    }

    /**
     * Set the price
     *
     * @param  mixed $price
     * @return void
     */
    public function setPriceAttribute($price = 0)
    {
        $this->attributes['amount'] = $price;
    }

    /**
     * Get the price
     *
     * @return void
     */
    public function getPointAttribute()
    {
        return $this->points;
    }

    /**
     * Set the price
     *
     * @param  mixed $price
     * @return void
     */
    public function setPointAttribute($price = 0)
    {
        $this->attributes['points'] = $price;
    }

    /**
     * getUserId
     *
     * @return int|string
     */
    public function getUserId(): int|string
    {
        return $this->payee_id;
    }

    /**
     * getPoints
     *
     * @return void
     */
    public function getPoints(): int|string
    {
        return $this->points;
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public static function newFactory()
    {
        return TransactionFactory::new();
    }
}