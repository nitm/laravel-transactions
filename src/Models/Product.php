<?php

namespace Nitm\Transactions\Models;

use CloudCreativity\LaravelStripe\Facades\Stripe;
use Database\Factories\Nitm\Transactions\Models\ProductFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Nitm\Content\Models\Model as Model;
use Nitm\Content\Traits\Sluggable;
use Nitm\Transactions\Contracts\Models\Product as ModelsProduct;

/**
 * @SWG\Definition(
 *      definition="Product",
 *      required={"title", "price", "slug"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="image",
 *          description="image",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="stripe_product_id",
 *          description="stripe_product_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Product extends Model implements ModelsProduct
{
    use SoftDeletes, Sluggable;

    public $table = 'products';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $slugs = ['slug' => 'title'];

    protected $createdByAuthFields = [];

    protected $hidden = ['stripe_product_id'];

    protected $attributes = [
        'price'  => 0,
        'points' => 0,
    ];

    public $fillable = [
        'title',
        'price',
        'points',
        'description',
        'image',
        'slug',
        'stripe_product_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'title'             => 'string',
        'price'             => 'float',
        'points'            => 'integer',
        'description'       => 'string',
        'image'             => 'string',
        'slug'              => 'string',
        'stripe_product_id' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title'             => 'required|string|max:255',
        'price'             => 'required|numeric',
        'description'       => 'nullable|string|max:255',
        'image'             => 'nullable|string|max:255',
        'slug'              => 'required|string|max:255',
        'stripe_product_id' => 'nullable|string|max:255',
        'deleted_at'        => 'nullable',
        'created_at'        => 'nullable',
        'updated_at'        => 'nullable',
    ];

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'stripe_product_id', 'stripe_product_id');
    }

    public function getCurrencyAttribute()
    {
        return strtolower(Arr::get($this->attributes, 'currency', 'usd'));
    }

    /**
     * Get The Stripe Product associated with this Product
     *
     * @return void
     */
    public function getProduct()
    {
        if (app()->environment('testing')) {
            return app('nitm.stripe')->account()->products()->create($this->title);
        }
        return app('nitm.stripe')->account()->products()->expand(['prices'])->retrieve($this->stripe_product_id);
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return ProductFactory::new();
    }
}