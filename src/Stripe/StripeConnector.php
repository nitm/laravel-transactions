<?php

/**
 * Expanded strip service with support for other stripe methods
 *
 * @author Malcolm Paul <malcolm@ninjasitm.com>
 */

namespace Nitm\Transactions\Stripe;

use CloudCreativity\LaravelStripe\Client;
use Nitm\Transactions\Repositories\StripePriceRepository;
use Nitm\Transactions\Repositories\StripeProductRepository;
use Nitm\Transactions\Repositories\StripeCustomerRepository;
use CloudCreativity\LaravelStripe\Connector as LaravelStripeStripeConnector;

class StripeConnector extends LaravelStripeStripeConnector
{
    /**
     * @return StripeProductRepository
     */
    public function products(): StripeProductRepository
    {
        return new StripeProductRepository(
            app(Client::class),
            $this->accountId()
        );
    }

    /**
     * @return StripePriceRepository
     */
    public function prices(): StripePriceRepository
    {
        return new StripePriceRepository(
            app(Client::class),
            $this->accountId()
        );
    }

    /**
     * @return StripeCustomerRepository
     */
    public function customers(): StripeCustomerRepository
    {
        return new StripeCustomerRepository(
            app(Client::class),
            $this->accountId()
        );
    }
}