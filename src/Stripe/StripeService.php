<?php

/**
 * Expanded strip service with support for other stripe methods
 *
 * @author Malcolm Paul <malcolm@ninjasitm.com>
 */

namespace Nitm\Transactions\Stripe;

use CloudCreativity\LaravelStripe\StripeService as LaravelStripeStripeService;

class StripeService extends LaravelStripeStripeService
{

    /**
     * Access the main application account.
     *
     * @return Connector
     */
    public function account()
    {
        return new StripeConnector();
    }
}