<?php

namespace Nitm\Transactions\Facades;

use Stripe\StripeObject;
use Nitm\Transactions\Testing\StripeFake;
use Illuminate\Support\Facades\Facade;
use CloudCreativity\LaravelStripe\Testing\ClientFake;
use CloudCreativity\LaravelStripe\Facades\Stripe as StripeFacade;

/**
 * ReadyUp Facade
 */
class Stripe extends StripeFacade
{
    /**
     * Fake static calls to Stripe.
     *
     * @param StripeObject ...$queue
     * @return void
     */
    public static function fake(StripeObject ...$queue)
    {
        /**
         * Swapping the client stubs static calls to Stripe. This allows the entire Laravel
         * Stripe package to operate, with only the static calls to the Stripe package being
         * removed.
         */
        static::$app->instance(
            Client::class,
            $client = new ClientFake(static::$app->make('events'))
        );

        /**
         * We then swap in a Stripe service fake, that has our test assertions on it.
         * This extends the real Stripe service and doesn't overload anything on it,
         * so the service will operate exactly as expected.
         */
        static::swap($fake = new StripeFake($client));

        $fake->withQueue(...$queue);
    }

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'nitm.stripe';
    }
}