<?php

namespace Nitm\Transactions\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * ReadyUp Facade
 */
class Transaction extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'nitm-transaction';
    }
}