<?php

use Illuminate\Support\Facades\Route;
use Nitm\Transactions\Http\Controllers\API\TransactionsAPIController;
use Nitm\Transactions\Stripe\StripeService;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware(config('nitm-transactions.routes.middleware'))
    ->prefix(config('nitm-transasctions.routes.prefix', 'api/transactions'))
    ->as(config('nitm-transasctions.routes.prefix', 'transactions') . '.')
    ->group(
        function () {
            Route::get('form-config', [TransactionsAPIController::class, 'formConfig'])->name('config');
            Route::get('', [TransactionsAPIController::class, 'transactions'])->name('transactions');
            Route::post('', [TransactionsAPIController::class, 'paymentIntent'])->name('start');
            Route::post('/refund/{transaction}', [TransactionsAPIController::class, 'refund'])->name('refund');
        }
    );

/**
 * Payment auth routes
 */
$service = new StripeService;
$service->oauth('transactions/stripe/connect/authorize');
$service->oauth('transactions/stripe/connect/authorize/mobile');
$service->webhook('transactions/webhook', 'app');
$service->webhook('transactions/webhook/connect', 'connect');