<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Nitm\Transactions\Models\Transaction;

class TransactionTest extends TestCase
{
    // use RefreshDatabase;

    public function testCreate()
    {
        $data = Transaction::factory()->make();
        $model = Transaction::factory()->create($data->getAttributes());

        $this->assertInstanceOf(Transaction::class, $model);
        $this->assertGreaterThan(0, $model->id);
    }

    public function testUpdate()
    {
        $model = Transaction::factory()->create();
        $data = $model->getAttributes();

        $updateData = Transaction::factory()->make();
        $model->fill($updateData->toArray());
        $keys = array_keys($updateData->toArray());

        $this->assertInstanceOf(Transaction::class, $model);
        $this->assertNotEquals(
            Arr::only($data, $keys),
            Arr::only($model->getAttributes(), $keys)
        );
    }

    public function testDelete()
    {
        $model = Transaction::factory()->create();

        $model->delete();
        $this->assertNotNull($model->deleted_at);

        $model->forceDelete();
        $this->assertDatabaseMissing($model->getTable(), ['id' => $model->id]);

        $model->delete();

        $this->assertInstanceOf(Transaction::class, $model);
        $this->assertDatabaseMissing($model->getTable(), ['id' => $model->id]);
    }
}