<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Nitm\Transactions\Models\Product;

class ProductTest extends TestCase
{
    // use RefreshDatabase;

    public function testCreate()
    {
        $data = Product::factory()->make();
        $model = Product::factory()->create($data->getAttributes());

        $this->assertInstanceOf(Product::class, $model);
        $this->assertGreaterThan(0, $model->id);
    }

    public function testUpdate()
    {
        $model = Product::factory()->create();
        $data = $model->getAttributes();

        $updateData = Product::factory()->make();
        $model->fill($updateData->toArray());
        $keys = array_keys($updateData->toArray());

        $this->assertInstanceOf(Product::class, $model);
        $this->assertNotEquals(
            Arr::only($data, $keys),
            Arr::only($model->getAttributes(), $keys)
        );
    }

    public function testDelete()
    {
        $model = Product::factory()->create();

        $model->delete();
        $this->assertNotNull($model->deleted_at);

        $model->forceDelete();
        $this->assertDatabaseMissing($model->getTable(), ['id' => $model->id]);

        $model->delete();

        $this->assertInstanceOf(Product::class, $model);
        $this->assertDatabaseMissing($model->getTable(), ['id' => $model->id]);
    }
}