<?php

namespace Tests\APIs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Nitm\Transactions\Models\Transaction;

class TransactionApiTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     */
    public function test_start_payment_intent()
    {
        $this->useAs('user');
        $transaction = Transaction::factory()->withProduct()->withStripeDetails()->create();

        $this->response = $this->json(
            'POST',
            '/api/transactions',
            $transaction->toArray()
        );

        $this->assertApiSuccess();
    }
}