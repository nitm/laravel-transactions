<?php

namespace Tests\Repositories;

use Tests\TestCase;
use Nitm\Content\Models\User;
use Tests\ApiTestTrait;
use Nitm\Transactions\Models\Transaction;
use Nitm\Transactions\Gamify\Points\Gift;
use Nitm\Transactions\Gamify\Points\Purchase;
use Nitm\Transactions\Repositories\TransactionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TransactionRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var TransactionRepository
     */
    protected $transactionRepo;

    public function setUp(): void
    {
        parent::setUp();
        $this->transactionRepo = \App::make(TransactionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_transaction()
    {
        $transaction = Transaction::factory()
            ->withProduct()
            ->make()
            ->toArray();

        $createdTransaction = $this->transactionRepo->create($transaction);

        $createdTransaction = $createdTransaction->toArray();
        $this->assertArrayHasKey('id', $createdTransaction);
        $this->assertNotNull($createdTransaction['id'], 'Created Transaction must have id specified');
        $this->assertNotNull(Transaction::find($createdTransaction['id']), 'Transaction with given id must be in DB');
    }

    /**
     * @test gift
     */
    // public function test_gift()
    // {
    //     $pointClass = Gift::class;
    //     $user = auth()->user();
    //     $receiver = User::factory()->create();
    //     $startingPoints = 1000;
    //     $user->addPoint($startingPoints);
    //     $this->assertEquals(1000, $user->getPoints());
    //     $transaction = Transaction::factory()
    //         ->withProduct()
    //         ->withPayee($receiver)
    //         ->withPoints(new $pointClass($receiver))
    //         ->make()
    //         ->toArray();

    //     $createdTransaction = $this->transactionRepo->gift($transaction);

    //     $createdTransaction = $createdTransaction->toArray();
    //     $this->assertArrayHasKey('id', $createdTransaction);
    //     $this->assertNotNull($createdTransaction['id'], 'Created Transaction must have id specified');
    //     $this->assertNotNull(Transaction::find($createdTransaction['id']), 'Transaction with given id must be in DB');
    //     // dump([
    //     //     "Transaction" => $transaction,
    //     //     "Receiver" => $receiver->id,
    //     //     "Gift Transaction" => $createdTransaction,
    //     //     "reciever points" => $receiver->getPoints(),
    //     //     "user points" => $user->getPoints(),
    //     // ]);
    //     $this->assertEquals((int)$receiver->id, (int)$createdTransaction['payee_id']);
    //     $this->assertEquals((int)$user->id, (int)$createdTransaction['subject_id']);
    //     $this->assertEquals(User::class, $createdTransaction['subject_type']);
    //     $this->assertEquals($startingPoints + (int)$createdTransaction['point'], $user->getPoints());
    //     $this->assertEquals(-$createdTransaction['point'], $receiver->refresh()->getPoints());
    // }

    /**
     * @test refund
     */
    public function test_refund()
    {
        $user = auth()->user() ?: User::factory()->create();
        $transaction = Transaction::factory()
            ->withProduct()
            ->withStripeDetails()
            ->withPayee($user)
            ->make()
            ->toArray();

        $createdTransaction = $this->transactionRepo->create($transaction);

        $createdTransaction = $createdTransaction->toArray();
        $this->assertArrayHasKey('id', $createdTransaction);
        $this->assertNotNull($createdTransaction['id'], 'Created Transaction must have id specified');
        $this->assertNotNull(Transaction::find($createdTransaction['id']), 'Transaction with given id must be in DB');
        // dump([
        //     "Transaction" => $transaction,
        //     "Purchase Transaction" => $createdTransaction,
        //     "user points" => $user->getPoints(),
        //     "points" => $point->getPoints(),
        // ]);
        $this->assertEquals((int)$user->id, (int)$createdTransaction['payee_id']);

        $refundedTransaction = $this->transactionRepo->refund([
            'transaction_id' => $createdTransaction['id']
        ]);
    }

    /**
     * @test delete
     */
    public function test_delete_transaction()
    {
        $transaction = Transaction::factory()->create();

        $resp = $this->transactionRepo->delete($transaction->id);

        $this->assertTrue($resp);
        $this->assertNull(Transaction::find($transaction->id), 'Transaction should not exist in DB');
    }
}