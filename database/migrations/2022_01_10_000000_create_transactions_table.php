<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('transactions')) {
            Schema::create('transactions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('payee_id')->unsigned();
                $table->integer('product_id')->unsigned()->nullable();
                $table->float('amount', 8, 2)->default(0);
                $table->integer('points')->default(0);
                $table->string('currency')->default('USD');
                $table->string('stripe_product_id')->nullable();
                $table->string('stripe_payment_intent_id')->nullable();
                $table->string('stripe_charge_id')->nullable();
                $table->string('stripe_refund_id')->nullable();
                $table->string('stripe_invoice_id')->nullable();
                $table->json('meta')->nullable();
                $table->softDeletes();
                $table->timestamps();

                $table->foreign('payee_id')->references('id')->on('users');
                $table->foreign('product_id')->references('id')->on('products');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}