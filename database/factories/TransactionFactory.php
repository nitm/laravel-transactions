<?php

namespace Database\Factories\Nitm\Transactions\Models;

use Nitm\Content\Models\User;
use Nitm\Transactions\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Nitm\Transactions\Models\Transaction;
use CloudCreativity\LaravelStripe\Facades\Stripe;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transaction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $userClass = config('nitm-transactions.user_model');
        return [
            'payee_id' => $userClass::factory()->create()->id,
            'price'  => 100,
            'points' => 500,
        ];
    }

    /**
     * Include a payee for this transaction.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function withPayee(User $payee = null)
    {
        return $this->state(function (array $attributes) use ($payee) {
            return [
                'payee_id' => $payee ? $payee->id : User::factory()->create()->id,
            ];
        });
    }

    /**
     * Include a user for this transaction.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function withUser(User $payee = null)
    {
        return $this->state(function (array $attributes) use ($payee) {
            return [
                'user' => $payee ? $payee->id : User::factory()->create()->id,
            ];
        });
    }

    /**
     * Include a subject for this transaction
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function withSubject(Model $model)
    {
        return $this->state(function (array $attributes) use ($model) {
            return [
                'subject_type' => get_class($model),
                'subject_id' => $model->id,
            ];
        });
    }

    /**
     * Include a product for this transaction.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function withProduct(Model $product = null)
    {
        $product = $product ?: Product::factory()->create();
        return $this->state(function (array $attributes) use ($product) {
            return [
                'product_id' => $product->id,
                'point' => $product->points,

            ];
        });
    }

    /**
     * Include a stripe details for this transaction.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function withStripeDetails(Model $product = null)
    {
        $product = $product ?: Product::factory()->withStripeProductId()->create();
        return $this->state(function (array $attributes) use ($product,) {
            Stripe::fake(
                $stripeProduct = new \Stripe\Product,
                $paymentIntent = new \Stripe\PaymentIntent,
            );
            return [
                'product_id' => $product->id,
                'stripe_product_id' => $stripeProduct->id,
                'stripe_payment_intent_id' => $paymentIntent->id
            ];
        });
    }
}