<?php
/**
 * Copyright 2020 Cloud Creativity Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @var \Illuminate\Database\Eloquent\Factory $factory */

namespace Database\Factories\Nitm\Transactions\Models;

use Nitm\Transactions\Models\StripeAccount;
use CloudCreativity\LaravelStripe\Config;
use Illuminate\Database\Eloquent\Factories\Factory;

class StripeAccountFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StripeAccount::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id'                => $this->faker->unique()->lexify('acct_????????????????'),
            'country'           => $this->faker->randomElement(['AU', 'GB', 'US']),
            'default_currency'  => $this->faker->randomElement(Config::currencies()->all()),
            'details_submitted' => $this->faker->boolean(75),
            'email'             => $this->faker->email,
            'payouts_enabled'   => $this->faker->boolean(75),
            'type'              => $this->faker->randomElement(['standard', 'express', 'custom']),
        ];
    }
}
