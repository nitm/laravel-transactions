<?php

namespace Database\Factories\Nitm\Transactions\Models;

use Nitm\Transactions\Models\Product;
use CloudCreativity\LaravelStripe\Facades\Stripe;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence,
            'slug' => $this->faker->slug,
            'price' => 100,
        ];
    }

    /**
     * Include a product for this transaction.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function withStripeProductId()
    {
        return $this->state(function (array $attributes) {
            Stripe::fake(
                $stripeProduct = new \Stripe\Product,
            );
            return [
                'stripe_product_id' => $stripeProduct->id,
            ];
        });
    }
}
