<?php
/**
 * Copyright 2020 Cloud Creativity Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** @var Factory $factory */

namespace Database\Factories\Nitm\Transactions\Models;

use Nitm\Transactions\Models\StripeEvent;
use CloudCreativity\LaravelStripe\Models\StripeAccount;
use Illuminate\Database\Eloquent\Factories\Factory;

class StripeEventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StripeEvent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id'               => $this->faker->unique()->lexify('evt_????????????????'),
            'api_version'      => $this->faker->date(),
            'created'          => $this->faker->dateTimeBetween('-1 hour', 'now'),
            'livemode'         => $this->faker->boolean,
            'pending_webhooks' => $this->faker->numberBetween(0, 100),
            'type'             => $this->faker->randomElement([
                'charge.failed',
                'payment_intent.succeeded',
            ]),
        ];
    }

    public function connect()
    {
        return $this->state(
            StripeEvent::class, 'connect', function () {
                return [
                    'account_id' => StripeAccount::factory()->create()->getKey(),
                ];
            });
    }
}
