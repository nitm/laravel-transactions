<?php

namespace Nitm\Transactions\Database\Seeders;

use Nitm\Transactions\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::factory()->count(5)->create();
    }
}