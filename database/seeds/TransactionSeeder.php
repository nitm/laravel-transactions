<?php

namespace Nitm\Transactions\Database\Seeders;

use Nitm\Transactions\Models\Transaction;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!Product::count()) {
            Product::factory()->count(5)->create();
        }
        Product::get()->map(function ($product) {
            $product->transactions()->saveMany(Transaction::factory()->count(5)->make());
        });
    }
}