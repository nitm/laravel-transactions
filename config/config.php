<?php

return [
    /**
     * User model
     */
    'user_model' => env('NITM_TRANSACTIONS_USER_MODEL', 'Nitm\Content\Models\User'),

    /**
     * Routes configuration
     */
    'routes' => [
        'prefix' => env('NITM_TRANSACTIONS_ROUTES_PREFIX', 'transactions'),
        'middleware' => env('NITM_TRANSACTIONS_ROUTES_MIDDLEWARE', ['api']),
    ],

    /**
     * Nova configuration support
     */
    'nova' => [
        'user' => Nitm\Transactions\Nova\User::class,
    ]
];